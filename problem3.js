// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
// Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


let sortedByCarModel = (inventory) => {
    
    if (!inventory || inventory.length < 1) {
        return [];
      }

    let sortedByalpha = [...inventory];

    return sortedByalpha.sort((a ,b) => {
        let fa = a.car_model.toLowerCase(),
            fb = b.car_model.toLowerCase();
    
    if (fa < fb) {
        return -1;
    }
    if (fa > fb) {
        return 1;
    }
    return 0;
    });
    
}

module.exports = sortedByCarModel;