const getCarById = require("../problem1");
const inventory = require("../inventory");

let carDetail = getCarById(inventory , 45);

if (Array.isArray(carDetail)) {
  console.log(carDetail);
} else {
  console.log(
    `car ${carDetail.id} is a ${carDetail.car_year} ${carDetail.car_make} ${carDetail.car_model}`
  );
}
