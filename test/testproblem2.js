const inventory = require("../inventory");
const getLastCar = require("../problem2");

let lastCar = getLastCar(inventory);

if (Array.isArray(lastCar)) {
  console.log(lastCar);
} else {
  console.log(`last car is a ${lastCar.car_make} ${lastCar.car_model}`);
}
