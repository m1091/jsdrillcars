// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.
//Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


let BmwAudicars = (inventory) => {
 
  if (!inventory || inventory.length < 1) {
    return [];
  }
  
  let selectedarr = [];
  for (let i = 0; i < inventory.length; i++) {
    let carMake = inventory[i].car_make.toLowerCase();
    if (carMake === "bmw" || carMake === "audi") {
      selectedarr.push(inventory[i]);
    }
  }
  return selectedarr;
};

module.exports = BmwAudicars;