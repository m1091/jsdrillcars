// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot.
//Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

//function to get all car years

let getCarYear = (inventory) => {

  if (!inventory || inventory.length < 1 ) {
    return [];
  }

  let arrYears = [];
  for (let i = 0; i < inventory.length; i++) {
    arrYears.push(inventory[i].car_year);
  }
  return arrYears;
};

module.exports = getCarYear;